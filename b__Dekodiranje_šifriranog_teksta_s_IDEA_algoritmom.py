Python 3.10.1 (tags/v3.10.1:2cd268a, Dec  6 2021, 19:10:37) [MSC v.1929 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
from Crypto.Cipher import IDEA
def decrypt_text(cipher_text, key):

cipher = IDEA.new(key.encode(), IDEA.MODE_ECB)

decrypted_text = cipher.decrypt(cipher_text)

unpadded_text = decrypted_text[:-decrypted_text[-1]]

return unpadded_text.decode()


cipher_text = b'\x87\xd3\xba\xed\xd1F\x9b\x88\xb2\xb3\x0f\x8e\x15U'
key = "tajni_kljuc123"
decrypted_text = decrypt_text(cipher_text, key)
print(decrypted_text)