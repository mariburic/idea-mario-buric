from Crypto.Cipher import IDEA

from Crypto.Random import get_random_bytes



def generate_key():

    key = get_random_bytes(16)

    return key

def encrypt_file(file_path, key):

    with open(file_path, 'rb') as file:

        data = file.read()

    cipher = IDEA.new(key, IDEA.MODE_ECB)

    padded_data = data + ((8 - len(data) % 8) * b'\x08')

    encrypted_data = cipher.encrypt(padded_data)

    with open(file_path + ".enc", 'wb') as file:

        file.write(encrypted_data)

key = generate_key()

file_path = "test.txt"

encrypt_file(file_path, key)
