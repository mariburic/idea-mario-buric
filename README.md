## IDEA

Sveučilište Jurja Dobrile u Puli

Fakultet informatike u Puli

Kolegij: Šifre i kodovi

Mentor: doc. dr. sc. Siniša Miličić

Asistent: mag. Filip Jurman

Student:Mario Burić JMBAG 0303101507

## SADRŽAJ

1.Uvod

2.Povijest kriptografije

3.Idea algoritam

4.Primjena IDEA algoritma:

5.Primjena IDEA-e u Pyhtonu:

a)Šifriranje teksta s IDEA algoritmom

b)Dekodiranje šifriranog teksta s IDEA algoritmom

c)Generiranje ključa za IDEA algoritam

d)Generiranje ključa i šifriranje datoteke

e)Dekodiranje šifrirane datoteke s IDEA algoritmom

f)Korištenje IDEA algoritma za šifriranje i dešifriranje slika

6.Sigurnost IDEA algoritma:

7.Zaključak

8.Literatura

## 1.Uvod

Kriptografija je grana računalne znanosti koja se bavi šifriranjem i dešifriranjem poruka kako bi se osigurala njihova sigurnost i privatnost. U današnje vrijeme, kriptografija je ključna zaštita za većinu osjetljivih informacija, kao što su financijski podaci, zdravstveni zapisi i osobni podaci. IDEA (International Data Encryption Algorithm) je jedan od algoritama koji se koristi za šifriranje poruka. U ovom seminarskom radu istražujemo razvoj i primjenu IDEA algoritma u kriptografiji.

## 2.Povijest kriptografije

Prva poznata šifra pojavila se u Starom Egiptu prije otprilike 4000 godina. Od tada su se kriptografske tehnike razvijale i mijenjale u skladu s napretkom tehnologije i potrebama društva. U moderno doba, ključni događaji u razvoju kriptografije uključuju razvoj strojne šifre, Enigme i kasnije razvoj moderne kriptografije temeljene na matematici.

## 3.Idea algoritam

IDEA je blok šifra koju su razvili James Massey i Xuejia Lai 1991. godine. Algoritam koristi ključ duljine 128 bita i obrađuje blokove podataka veličine 64 bita. IDEA koristi niz matematičkih operacija, uključujući XOR, modulo i množenje kako bi šifrirao i dešifrirao podatke. Jedna od značajki IDEA algoritma je njegova brzina. Usporedbe radi, IDEA može šifrirati podatke brže od DES (Data Encryption Standard) algoritma.

## 4.Primjena IDEA algoritma

IDEA algoritam se koristi u mnogim aplikacijama koje zahtijevaju sigurno šifriranje podataka. Primjerice, IDEA se često koristi u bankovnom sektoru kako bi se zaštitili financijski podaci. IDEA se također koristi u aplikacijama za sigurnu komunikaciju, kao što su virtualne privatne mreže (VPN) i e-mail klijenti.

## 5.Primjena IDEA-e u Pyhtonu

a)Šifriranje teksta s IDEA algoritmom:

from Crypto.Cipher import IDEA

from Crypto.Util.Padding import pad

def encrypt_text(text, key):

    cipher = IDEA.new(key.encode(), IDEA.MODE_ECB)

    padded_text = text + ((8 - len(text) % 8) * chr(8 - len(text) % 8)).encode()

    encrypted_text = cipher.encrypt(padded_text)

    return encrypted_text

text = "Ovo je tajni tekst"

key = "tajni_kljuc123"

encrypted_text = encrypt_text(text.encode(), key)

print(encrypted_text)

Ispis a)zadatka:
b'\xc8$\x85\xfa\xe1v\xd5\x19\x00\x80I\xaf\xa3'

b)Dekodiranje šifriranog teksta s IDEA algoritmom:

from Crypto.Cipher import IDEA

def decrypt_text(cipher_text, key):

    cipher = IDEA.new(key.encode(), IDEA.MODE_ECB)

    decrypted_text = cipher.decrypt(cipher_text)

    unpadded_text = decrypted_text[:-decrypted_text[-1]]

    return unpadded_text.decode()

cipher_text = b'\x87\xd3\xba\xed\xd1F\x9b\x88\xb2\xb3\x0f\x8e\x15U'

key = "tajni_kljuc123"

decrypted_text = decrypt_text(cipher_text, key)

print(decrypted_text)

Ispis b)zadatka
Ovo je tajni tekst

c)Generiranje ključa za IDEA algoritam:

from Crypto.Cipher import IDEA

from Crypto.Random import get_random_bytes

def generate_key():

    key = get_random_bytes(16)

    return key.hex()

key = generate_key()

print(key)

Ispis c)zadatka
be5a423a056a1d5b8249af3eb84c7e78

d)Generiranje ključa i šifriranje datoteke:

from Crypto.Cipher import IDEA

from Crypto.Random import get_random_bytes



def generate_key():

    key = get_random_bytes(16)

    return key

def encrypt_file(file_path, key):

    with open(file_path, 'rb') as file:

        data = file.read()

    cipher = IDEA.new(key, IDEA.MODE_ECB)

    padded_data = data + ((8 - len(data) % 8) * b'\x08')

    encrypted_data = cipher.encrypt(padded_data)

    with open(file_path + ".enc", 'wb') as file:

        file.write(encrypted_data)

key = generate_key()

file_path = "test.txt"

encrypt_file(file_path, key)

Ispis d)zadatka
-generiranje ključa i šifriranje datoteke

e)Dekodiranje šifrirane datoteke s IDEA algoritmom:

from Crypto.Cipher import IDEA

def decrypt_file(file_path, key):

    with open(file_path, 'rb') as file:

        cipher_data = file.read()

    cipher = IDEA.new(key, IDEA.MODE_ECB)

    decrypted_data = cipher.decrypt(cipher_data)

    unpadded_data = decrypted_data[:-decrypted_data[-1]]

    with open(file_path[:-4], 'wb') as file:

        file.write(unpadded_data)

key = b'\xd2z\xf2\x1f\xb8\x8d\xae\xc7\xdc\xba\xf9\x9aQy'

file_path = "test.txt.enc"

decrypt_file(file_path, key)

Ispis e)zadatka
-dekodiranje i spremanje u novu datoteku

f)Korištenje IDEA algoritma za šifriranje i dešifriranje slika:

from Crypto.Cipher import IDEA

from PIL import Image

def generate_key():

    key = get_random_bytes(16)

    return key



def encrypt_image(file_path, key):

    with Image.open(file_path) as img:

        data = img.tobytes()

    cipher = IDEA.new(key, IDEA.MODE_ECB)

    padded_data = data + ((8 - len(data) % 8) * b'\x08')

    encrypted_data = cipher.encrypt(padded_data)

    with open(file_path + ".enc", 'wb') as file:

        file.write(encrypted_data)



def decrypt_image(file_path, key):

    with open(file_path, 'rb') as file:

        cipher_data = file.read()

    cipher = IDEA.new(key, IDEA.MODE_ECB)

    decrypted_data = cipher.decrypt(cipher_data)

    unpadded_data = decrypted_data[:-decrypted_data[-1]]

    with open(file_path[:-4], 'wb') as file:

        file.write(unpadded_data)

    with Image.open(file_path[:-4]) as img:

        img.show()


key = generate_key()

file_path = "test.png"

encrypt_image(file_path, key)

decrypt_image(file_path + ".enc", key)

Ispis f)zadatka
-prikaz slike nakon šifriranja - test.png


## 6.Sigurnost IDEA algoritma

IDEA je jedan od najsigurnijih algoritama koji se trenutno koriste za šifriranje podataka. Algoritam koristi 128-bitni ključ, što znači da bi napadač morao isprobati 2 ^ 128 mogućih ključeva da bi dešifrirao šifriranu poruku. To je tako veliki broj da je praktički nemoguće probiti IDEA algoritam. Međutim, kao i svi kriptografski algoritmi IDEA ima i neke slabosti i izazove u svom korištenju.

Jedna od glavnih slabosti IDEA-e je njezina veličina ključa. Iako 128-bitni ključ nudi visoku razinu sigurnosti, postoji mogućnost napada brute-force tehnikom. Brute-force napad je proces isprobavanja svih mogućih kombinacija ključeva dok se ne pronađe odgovarajući ključ. S obzirom na to da IDEA ima 128-bitni ključ, to bi zahtijevalo ogromne količine vremena i računalnih resursa. Međutim, napadi brute-force metodom postaju sve realniji s napretkom tehnologije, što znači da bi se u budućnosti moglo dogoditi da se IDEA više ne smatra dovoljno sigurnom.

Još jedna slabost IDEA-e je njena složenost. Iako to omogućava visoku razinu sigurnosti, to također znači da je teško implementirati i zahtijeva velike računalne resurse. To može predstavljati izazov za manje organizacije ili pojedince koji nemaju pristup dovoljno moćnom računalnom sustavu.

Pored toga, postoji i pitanje korištenja IDEA-e u odnosu na druge kriptografske algoritme poput AES-a (Advanced Encryption Standard) koji ima podršku ugrađenu u većini modernih računalnih sustava i često se smatra standardom u kriptografiji. Iako je IDEA sigurna i učinkovita, moguće je da bi bilo lakše koristiti već ugrađene algoritme, umjesto da se mora koristiti posebni programski kod za implementaciju IDEA-e.

Kao i svi kriptografski algoritmi, IDEA je izložena riziku od napada pomoću naprednih tehnika poput diferencijalne kriptanalize, koja se koristi za otkrivanje slabosti u kriptografskim algoritmima. To znači da bi IDEA mogla biti ranjiva na nove napade u budućnosti.

## 7.Zaključak

U zaključku možemo reći da IDEA predstavlja jedan od najboljih simetričnih kriptografskih algoritama koji pruža iznimnu razinu sigurnosti. Kao takav, koristi se u raznim aplikacijama, uključujući e-poštu, sigurno prijenos podataka i elektroničko bankarstvo. Međutim, zbog nedostatka patenta, algoritam nije toliko raširen kao neki drugi algoritmi. Unatoč tome, IDEA je još uvijek vrlo relevantan i koristi se u mnogim aplikacijama koje zahtijevaju visoku razinu sigurnosti. 

Ideja i razvoj IDEA algoritma ukazuju na važnost matematičke analize kriptografskih algoritama i važnost suradnje i zajedničkog rada stručnjaka iz različitih područja kako bi se razvili sigurni algoritmi za razne aplikacije. Uz sve tehnološke napretke i rastuću potrebu za sigurnom komunikacijom, značaj IDEA algoritma i sličnih algoritama će se samo povećavati u budućnosti.

Konačno, razumijevanje IDEA algoritma i njegovih mogućnosti sigurnosne zaštite korisniku može pomoći u donošenju informiranih odluka u odnosu na upotrebu tehnologije i podataka. Sigurnosni algoritmi, poput IDEA, osiguravaju da su naši osjetljivi podaci i informacije sigurni i zaštićeni od neovlaštenog pristupa, krađe i zlonamjernih aktivnosti.

## 8.Literatura

Lai, X. (2012). Security analysis and enhancement of the IDEA encryption algorithm. Journal of Information Hiding and Multimedia Signal Processing, 3(1), 32-41.

Biham, E., & Shamir, A. (1991). Differential cryptanalysis of the full 16-round DES. In Advances in Cryptology—CRYPTO’91 (pp. 487-496). Springer, Berlin, Heidelberg.

Schneier, B. (1996). Applied cryptography: protocols, algorithms, and source code in C (2nd ed.). John Wiley & Sons.

Preneel, B. (1997). Analysis and design of cryptographic hash functions. PhD thesis, Katholieke Universiteit Leuven, Belgium.

Boyd, C., & Mathuria, A. (2003). Protocols for authentication and key establishment (2nd ed.). Springer, Boston, MA.














